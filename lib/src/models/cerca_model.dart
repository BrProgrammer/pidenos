import 'package:meta/meta.dart' show required;

class CercaDeTiModel {
  final String path;
  final String lugar;
  final String tipo;
  CercaDeTiModel(
      {@required this.path, @required this.lugar, @required this.tipo});
}
