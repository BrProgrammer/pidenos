import 'package:meta/meta.dart' show required;

class RegisterModel {
  final String title;
  final String description;
  final String textBotton;
  final String text;
  final bool isCode;
  final double width;
  final double height;
  final double top;
  final double left;

  RegisterModel({
    @required this.title,
    @required this.description,
    @required this.textBotton,
    @required this.text,
    @required this.isCode,
    @required this.width,
    @required this.height,
    @required this.top,
    @required this.left,
  });
}
