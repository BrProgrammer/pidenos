import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';

class MenuHorizontal extends StatelessWidget {
  final List<Widget> widgets;
  final bool top;

  MenuHorizontal({Key key, @required this.widgets, this.top = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return SafeArea(
      top: top,
      bottom: false,
      child: Container(
        width: _responsive.width,
        padding: EdgeInsets.fromLTRB(
            _responsive.wp(7), _responsive.hp(1), _responsive.wp(8), 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: this.widgets,
        ),
      ),
    );
  }
}
