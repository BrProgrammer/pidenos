import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Slideshow extends StatelessWidget {
  final List<Widget> slides;
  final Color colorPrimario;
  final Color colorSecundario;
  final double bulletPrimario;
  final double bulletSecundario;
  final bool isRegister;
  Slideshow({
    @required this.slides,
    this.colorPrimario = Colors.blue,
    this.colorSecundario = Colors.grey,
    this.bulletPrimario = 12,
    this.bulletSecundario = 12,
    this.isRegister = false,
  });

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => new SlideshowModel(),
      child: SafeArea(
        child: Center(
          child: Builder(
            builder: (BuildContext context) {
              Provider.of<SlideshowModel>(context).colorPrimario =
                  this.colorPrimario;
              Provider.of<SlideshowModel>(context).colorSecundario =
                  this.colorSecundario;
              Provider.of<SlideshowModel>(context).bulletPrimario =
                  this.bulletPrimario;
              Provider.of<SlideshowModel>(context).bulletSecundario =
                  this.bulletSecundario;
              Provider.of<SlideshowModel>(context).isRegister = this.isRegister;
              return _CrearEstructuraSlideshow(
                slides: slides,
              );
            },
          ),
        ),
      ),
    );
  }
}

class _CrearEstructuraSlideshow extends StatelessWidget {
  const _CrearEstructuraSlideshow({
    Key key,
    @required this.slides,
  }) : super(key: key);

  final List<Widget> slides;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: _Slides(slides),
        ),
        _Dots(slides.length),
      ],
    );
  }
}

class _Dots extends StatelessWidget {
  final int totalSlides;

  _Dots(
    this.totalSlides,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(
          totalSlides,
          (i) => _Dot(i),
        ),
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  final int index;

  _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    final ssModel = Provider.of<SlideshowModel>(context);
    double tamano;
    Color color;
    if (ssModel.currentPage >= index - 0.5 &&
        ssModel.currentPage < index + 0.5) {
      tamano = ssModel.bulletPrimario;
      color = ssModel.colorPrimario;
    } else {
      tamano = ssModel.bulletSecundario;
      color = ssModel.colorSecundario;
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      width: tamano,
      height: tamano,
      margin: EdgeInsets.symmetric(horizontal: 2.5),
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
    );
  }
}

class _Slides extends StatefulWidget {
  final List<Widget> slides;

  const _Slides(this.slides);

  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {
  final pageViewController = new PageController();

  @override
  void initState() {
    pageViewController.addListener(() {
      Provider.of<SlideshowModel>(context, listen: false).currentPage =
          pageViewController.page;
    });
    super.initState();
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<SlideshowModel>(
        builder: (_, slideshowModel, __) => PageView(
          physics: (slideshowModel.isRegister)
              ? NeverScrollableScrollPhysics()
              : BouncingScrollPhysics(),
          controller: slideshowModel.isRegister
              ? slideshowModel.pageController
              : pageViewController,
          children: widget.slides.map((slide) => _Slide(slide)).toList(),
        ),
      ),
    );
  }
}

class _Slide extends StatelessWidget {
  final Widget slide;

  const _Slide(this.slide);

  @override
  Widget build(BuildContext context) {
    return Consumer<SlideshowModel>(builder: (_, slides, __) {
      return Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all((slides.isRegister) ? 0 : 30),
        child: slide,
      );
    });
  }
}

class SlideshowModel with ChangeNotifier {
  double _currentPage = 0;
  Color _colorPrimario = Colors.blue;
  Color _colorSecundario = Colors.grey;
  double _bulletPrimario = 12;
  double _bulletSecundario = 12;
  bool _isRegister = false;

  int _paginaActual = 0;
  PageController _pageController = PageController();

  PageController get pageController => this._pageController;

  int get paginaActual => this._paginaActual;

  set paginaActual(int valor) {
    this._paginaActual = valor;
    this._currentPage = valor.toDouble();
    _pageController.animateToPage(
      valor,
      duration: Duration(milliseconds: 200),
      curve: Curves.easeOut,
    );
    notifyListeners();
  }

  double get currentPage => this._currentPage;

  set currentPage(double currentPage) {
    this._currentPage = currentPage;
    notifyListeners();
  }

  set isRegister(bool value) {
    this._isRegister = value;
  }

  bool get isRegister => this._isRegister;

  double get bulletPrimario => this._bulletPrimario;

  set bulletPrimario(double tamano) {
    this._bulletPrimario = tamano;
  }

  double get bulletSecundario => this._bulletSecundario;

  set bulletSecundario(double tamano) {
    this._bulletSecundario = tamano;
  }

  Color get colorPrimario => this._colorPrimario;

  set colorPrimario(Color color) {
    this._colorPrimario = color;
  }

  Color get colorSecundario => this._colorSecundario;

  set colorSecundario(Color color) {
    this._colorSecundario = color;
  }
}
