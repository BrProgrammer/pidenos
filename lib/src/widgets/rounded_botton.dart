import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class RoundedButton extends StatelessWidget {
  final String path;
  final String text;
  final Color color;
  final Function onPressed;
  final double width;
  final EdgeInsetsGeometry padding;

  const RoundedButton({
    Key key,
    this.path,
    @required this.text,
    this.color,
    @required this.onPressed,
    this.width,
    this.padding = EdgeInsets.zero,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);

    return CupertinoButton(
      padding: this.padding,
      child: Container(
        alignment: Alignment.center,
        width: this.width,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: this.color ?? Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (this.path != null)
              Image.asset(this.path,
                  width: _responsive.hp(2.6), color: Colors.white),
            if (this.path != null)
              SizedBox(
                width: _responsive.wp(3),
              ),
            TextWidget(
              text: this.text,
              color: Colors.white,
              fontSize: _responsive.ip(1.7),
              fontWeight: FontWeight.w600,
            ),
          ],
        ),
      ),
      onPressed: this.onPressed,
    );
  }
}
