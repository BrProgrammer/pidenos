import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';

class Logo extends StatelessWidget {
  final double top;
  final double porcentaje;

  const Logo({Key key, this.top, @required this.porcentaje}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: this.top ?? 0),
      alignment: Alignment.topCenter,
      child: Image.asset(
        'assets/icons/logo_horizontal.png',
        width: Responsive.of(context).width * this.porcentaje,
      ),
    );
  }
}
