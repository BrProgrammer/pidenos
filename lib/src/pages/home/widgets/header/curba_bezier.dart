import 'package:flutter/material.dart';
import 'package:pidenos/src/clippers/bezier_clipper.dart';

class CurbaBezier extends StatelessWidget {
  final double porcentaje;
  final double opacity;

  CurbaBezier({
    Key key,
    @required this.porcentaje,
    this.opacity = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: BezierClipper(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * this.porcentaje,
        color: Theme.of(context).primaryColor.withOpacity(this.opacity),
      ),
    );
  }
}
