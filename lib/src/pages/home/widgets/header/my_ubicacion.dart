import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/home/home_bloc.dart';
import 'package:pidenos/src/blocs/home/home_state.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class MyUbicacion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: MediaQuery.of(context).size.height * 0.13,
      left: MediaQuery.of(context).size.width * 0.1,
      right: MediaQuery.of(context).size.width * 0.1,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 35, vertical: 17),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [
            BoxShadow(
                color: Colors.black38,
                offset: Offset(0, 5),
                blurRadius: 7,
                spreadRadius: 1)
          ],
        ),
        child: Row(
          children: <Widget>[
            Icon(Icons.location_on, color: Colors.grey),
            Expanded(
              child: BlocBuilder<HomeBloc, HomeState>(
                builder: (_, state) => TextWidget(
                  text: (state.status == HomeStatus.selecting)
                      ? 'Activa tu localización'
                      : 'Santa Ana, Ecuador',
                  fontSize: Responsive.of(context).ip(1.95),
                  color: (state.status == HomeStatus.selecting)
                      ? Colors.grey
                      : Theme.of(context).accentColor,
                  fontWeight: (state.status == HomeStatus.selecting)
                      ? FontWeight.w500
                      : FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
