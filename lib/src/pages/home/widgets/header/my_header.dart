import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/home/widgets/header/curba_bezier.dart';
import 'package:pidenos/src/pages/home/widgets/header/my_icon.dart';
import 'package:pidenos/src/pages/home/widgets/header/my_ubicacion.dart';
import 'package:pidenos/src/widgets/logo.dart';
import 'package:pidenos/src/widgets/menu_horizontal.dart';

class MyHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
      child: Stack(
        children: <Widget>[
          CurbaBezier(porcentaje: .22, opacity: .3),
          CurbaBezier(porcentaje: .2),
          MenuHorizontal(widgets: <Widget>[
            Icon(Icons.menu, color: Colors.white, size: 35),
            Logo(porcentaje: .35),
            MyIcon(),
          ]),
          MyUbicacion(),
        ],
      ),
    );
  }
}
