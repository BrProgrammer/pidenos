import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IconNavigationBar extends StatelessWidget {
  final String path;
  final double width;

  IconNavigationBar({Key key, @required this.path, this.width})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: () {},
      padding: EdgeInsets.zero,
      child: Image.asset(
        this.path,
        width: this.width ?? 20,
        color: Colors.white,
      ),
    );
  }
}
