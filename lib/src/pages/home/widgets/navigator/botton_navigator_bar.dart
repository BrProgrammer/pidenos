import 'package:flutter/material.dart';
import 'package:pidenos/src/clippers/botton_navigator_bar_clipper.dart';
import 'package:pidenos/src/pages/home/widgets/navigator/icon_navigator_bar.dart';
import 'package:pidenos/src/utils/responsive.dart';

class MyBottonNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: ClipPath(
        clipper: BottonNavigatorClipper(),
        child: Container(
          width: Responsive.of(context).width,
          decoration: BoxDecoration(
            color: Theme.of(context).accentColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25), topRight: Radius.circular(25)),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    IconNavigationBar(path: 'assets/icons/noti.png'),
                    SizedBox(width: 20),
                    IconNavigationBar(path: 'assets/icons/location.png'),
                  ],
                ),
                Row(
                  children: <Widget>[
                    IconNavigationBar(path: 'assets/icons/bag.png'),
                    SizedBox(width: 20),
                    IconNavigationBar(
                        path: 'assets/icons/user2.png', width: 22),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
