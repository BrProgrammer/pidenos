import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/texts_lugares.dart';

class ImagenOpacity extends StatelessWidget {
  final String text;
  final String path;
  final bool isPanel;

  ImagenOpacity({
    Key key,
    @required this.text,
    @required this.path,
    this.isPanel = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      width: MediaQuery.of(context).size.width * .4,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(this.path, fit: BoxFit.cover),
          TextsOpacity(text: this.text, isPanel: this.isPanel),
        ],
      ),
    );
  }
}
