import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/resenias/imagen_resenias.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class Resenia extends StatelessWidget {
  final String path;
  final String usuario;
  final String restaurante;
  final String resenia;

  Resenia({
    Key key,
    @required this.path,
    @required this.usuario,
    @required this.restaurante,
    @required this.resenia,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Container(
      margin: EdgeInsets.only(
        left: _responsive.wp(9),
        top: _responsive.hp(2.5),
      ),
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ImagenResenia(path: this.path),
          SizedBox(width: 15),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextWidget(
                  text: this.usuario,
                  fontSize: _responsive.ip(2.1),
                ),
                TextWidget(
                  text: this.restaurante,
                  fontSize: _responsive.ip(1.8),
                  color: Color(0xffFFB700),
                ),
                SizedBox(height: 5),
                TextWidget(
                  text: this.resenia,
                  fontSize: _responsive.ip(1.6),
                  maxLines: 3,
                  textAlign: TextAlign.justify,
                  fontWeight: FontWeight.w300,
                ),
              ],
            ),
          ),
          SizedBox(width: _responsive.wp(9)),
        ],
      ),
    );
  }
}
