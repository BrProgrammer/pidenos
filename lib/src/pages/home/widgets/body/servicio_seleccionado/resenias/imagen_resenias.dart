import 'package:flutter/material.dart';

class ImagenResenia extends StatelessWidget {
  final String path;

  ImagenResenia({Key key, @required this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: ClipOval(
        child: Image.asset(
          this.path,
          width: MediaQuery.of(context).size.width * .15,
        ),
      ),
    );
  }
}
