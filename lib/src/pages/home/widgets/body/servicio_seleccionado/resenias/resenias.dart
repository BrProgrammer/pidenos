import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/home/home_bloc.dart';
import 'package:pidenos/src/blocs/home/home_state.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/resenias/resenia.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class Resenias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return BlocBuilder<HomeBloc, HomeState>(builder: (_, state) {
      return Column(
        children: <Widget>[
          TextWidget(
            text: 'Reseñas',
            fontSize: _responsive.ip(2.2),
            padding: EdgeInsets.only(top: _responsive.hp(3)),
            alignment: Alignment.bottomCenter,
          ),
          for (var i = 0; i < state.resenias.length; i++)
            Resenia(
              path: state.resenias[i].path,
              usuario: state.resenias[i].usuario,
              restaurante: state.resenias[i].restaurante,
              resenia: state.resenias[i].resenia,
            ),
        ],
      );
    });
  }
}
