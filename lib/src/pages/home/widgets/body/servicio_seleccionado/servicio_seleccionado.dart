import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/home/home_bloc.dart';
import 'package:pidenos/src/blocs/home/home_event.dart';
import 'package:pidenos/src/blocs/home/home_state.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/list_lugares_cercanos.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/resenias/resenias.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/menu_horizontal.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class ServicioSeleccionado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Container(
      width: _responsive.width,
      height: _responsive.height,
      padding: EdgeInsets.only(bottom: _responsive.hp(14)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: _responsive.hp(14)),
          BlocBuilder<HomeBloc, HomeState>(builder: (_, state) {
            return MenuHorizontal(
              top: false,
              widgets: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios,
                      color: Theme.of(context).accentColor, size: 30),
                  onPressed: () => HomeBloc.of(context).add(Selecting()),
                ),
                TextWidget(
                  text: state.tittleSelection,
                  fontSize: 25,
                ),
                IconButton(
                  padding: EdgeInsets.zero,
                  onPressed: () {},
                  icon: Icon(Icons.search,
                      color: Theme.of(context).accentColor, size: 30),
                )
              ],
            );
          }),
          SizedBox(height: _responsive.hp(1)),
          Expanded(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  ListaLugaresCercanos(),
                  Resenias(),
                  SizedBox(height: _responsive.hp(5))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
