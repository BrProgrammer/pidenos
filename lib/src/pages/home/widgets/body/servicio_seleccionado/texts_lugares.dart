import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class TextsOpacity extends StatelessWidget {
  final String text;
  final String text2;
  final bool isPanel;
  TextsOpacity({Key key, @required this.text, this.isPanel, this.text2})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Positioned.fill(
      child: Container(
        color: Colors.black.withOpacity(.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (!this.isPanel)
              TextWidget(
                text: this.text2,
                color: Colors.white,
                fontSize: _responsive.ip(1.5),
              ),
            TextWidget(
              text: this.text,
              fontSize: _responsive.ip(1.7),
              color: (this.isPanel)
                  ? Colors.white
                  : Theme.of(context).primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
