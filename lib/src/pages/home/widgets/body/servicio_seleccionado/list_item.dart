import 'package:flutter/material.dart';
import 'package:pidenos/src/clippers/cards_clipper.dart';
import 'package:pidenos/src/models/cerca_model.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/texts_lugares.dart';

class ListItem extends StatelessWidget {
  final int item;
  final CercaDeTiModel cerca;
  ListItem({
    Key key,
    @required this.item,
    @required this.cerca,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.only(left: (item == 0) ? 35 : 10),
        child: ClipPath(
          clipper: CardsClipper(border: 10),
          child: Container(
            width: MediaQuery.of(context).size.width * .4,
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Image.asset(this.cerca.path, fit: BoxFit.cover),
                TextsOpacity(
                    text: this.cerca.lugar,
                    isPanel: false,
                    text2: this.cerca.tipo),
              ],
            ),
          ),
        ),
      );
}
