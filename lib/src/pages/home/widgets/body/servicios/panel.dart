import 'package:flutter/material.dart';
import 'package:pidenos/src/blocs/home/home_bloc.dart';
import 'package:pidenos/src/blocs/home/home_event.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicios/item_panel.dart';
import 'package:pidenos/src/pages/home/widgets/body/imagen_opacity.dart';
import 'package:pidenos/src/utils/data.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class Panel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Container(
      width: _responsive.width,
      height: _responsive.height,
      child: Column(
        children: <Widget>[
          SizedBox(height: _responsive.hp(16)),
          TextWidget(
            text: 'Bienvenido Brayan \nNecesitas un servicio?',
            maxLines: 2,
            fontSize: _responsive.ip(2.7),
            letterSpacing: 1.5,
          ),
          SizedBox(height: _responsive.hp(1)),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                height: (_responsive.height < 700)
                    ? _responsive.height
                    : (_responsive.height < 800)
                        ? _responsive.hp(81)
                        : _responsive.hp(75),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: _responsive.hp(5)),
                    Expanded(
                      child: Stack(
                          children: panels
                              .map((panel) => ItemPanel(
                                    top: panel.top,
                                    left: panel.left,
                                    right: panel.right,
                                    child: (panel.text != null &&
                                            panel.path != null)
                                        ? ImagenOpacity(
                                            text: panel.text,
                                            path: panel.path,
                                          )
                                        : null,
                                    onPressed: (panel.resenias != null &&
                                            panel.cercas != null)
                                        ? () => HomeBloc.of(context).add(
                                              Recomendations(
                                                cercas: panel.cercas,
                                                resenias: panel.resenias,
                                                tittleSelection:
                                                    '${panel.text.substring(0, 1).toUpperCase()}${panel.text.substring(1)}',
                                              ),
                                            )
                                        : null,
                                  ))
                              .toList()),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
