import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math' as math;
import 'package:pidenos/src/clippers/polygon_panel/polygon_panel_clipper.dart';
import 'package:pidenos/src/utils/responsive.dart';

class ItemPanel extends StatefulWidget {
  final double top;
  final double left;
  final double right;
  final Widget child;
  final VoidCallback onPressed;

  ItemPanel({
    Key key,
    @required this.top,
    this.left,
    this.right,
    this.child,
    this.onPressed,
  }) : super(key: key);

  @override
  _ItemPanelState createState() => _ItemPanelState();
}

class _ItemPanelState extends State<ItemPanel>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _angle;

  @override
  void initState() {
    if (widget.child != null) {
      _controller = AnimationController(
          vsync: this, duration: Duration(milliseconds: 1200));
      _angle = TweenSequence(
        [
          TweenSequenceItem<double>(
              tween: Tween(begin: _toRadians(3), end: -_toRadians(3)),
              weight: 50),
          TweenSequenceItem<double>(
              tween: Tween(begin: -_toRadians(3), end: _toRadians(3)),
              weight: 50),
        ],
      ).animate(_controller);

      _controller.repeat();

      _angle.addListener(() {});
    }
    super.initState();
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  double _toRadians(double values) => values * math.pi / 180;

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Positioned(
      top: this.widget.top,
      left: this.widget.left,
      right: this.widget.right,
      child: ClipPolygon(
        borderRadius: 4.5,
        child: CupertinoButton(
          padding: EdgeInsets.zero,
          onPressed: widget.onPressed,
          pressedOpacity: .75,
          child: Container(
            width: _responsive.wp(34),
            height: _responsive.wp(34),
            color: Theme.of(context).primaryColor,
            child: (widget.child != null)
                ? AnimatedBuilder(
                    animation: _angle,
                    child: widget.child,
                    builder: (_, Widget child) => Transform.rotate(
                      angle: _angle.value,
                      child: child,
                    ),
                  )
                : null,
          ),
        ),
      ),
    );
  }
}
