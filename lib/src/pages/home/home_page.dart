import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/home/home_bloc.dart';
import 'package:pidenos/src/blocs/home/home_state.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicio_seleccionado/servicio_seleccionado.dart';
import 'package:pidenos/src/pages/home/widgets/body/servicios/panel.dart';
import 'package:pidenos/src/pages/home/widgets/floating_botton.dart';
import 'package:pidenos/src/pages/home/widgets/header/my_header.dart';
import 'package:pidenos/src/pages/home/widgets/navigator/botton_navigator_bar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomeBloc _homeBloc = HomeBloc();

  @override
  void dispose() {
    _homeBloc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _homeBloc,
      child: Scaffold(
        body: BlocBuilder<HomeBloc, HomeState>(builder: (_, state) {
          return Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Positioned(
                top: MediaQuery.of(context).size.height * 0.08,
                child: IndexedStack(
                  index: (state.status == HomeStatus.selecting) ? 0 : 1,
                  children: <Widget>[Panel(), ServicioSeleccionado()],
                ),
              ),
              MyHeader(),
              MyBottonNavigationBar(),
              MyFloatingActionButton(),
            ],
          );
        }),
      ),
    );
  }
}
