import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class Header extends StatelessWidget {
  final String title;
  final String description;
  Header({
    Key key,
    @required this.title,
    @required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Positioned(
      top: _responsive.height * .05,
      child: Container(
        width: _responsive.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            TextWidget(
              text: this.title,
              color: Theme.of(context).accentColor,
              fontSize: _responsive.ip(2.3),
              letterSpacing: 1.2,
              maxLines: 2,
            ),
            TextWidget(
              padding: EdgeInsets.only(top: 4),
              text: this.description,
              color: Theme.of(context).accentColor,
              fontSize: _responsive.ip(1.3),
              letterSpacing: .5,
              fontWeight: FontWeight.w700,
            ),
          ],
        ),
      ),
    );
  }
}
