import 'package:flutter/material.dart';
import 'package:pidenos/src/widgets/input_text_login.dart';

class InputCodigo extends StatelessWidget {
  final String number;

  InputCodigo({Key key, @required this.number}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: InputTextLogin(
        width: 29,
        initValue: this.number,
        isVerification: true,
        maxLength: 1,
        textInputType: TextInputType.number,
      ),
      color: Colors.white,
      padding: EdgeInsets.only(left: 7, top: 5, bottom: 5),
      margin: EdgeInsets.only(right: 8),
    );
  }
}
