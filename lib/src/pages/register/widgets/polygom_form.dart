import 'package:flutter/material.dart';
import 'package:pidenos/src/clippers/polygon_register_clipper.dart';
import 'package:pidenos/src/pages/login/widgets/item_phone.dart';
import 'package:pidenos/src/pages/register/widgets/input_codigo.dart';
import 'package:pidenos/src/utils/data.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/rounded_botton.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class PolygomForm extends StatelessWidget {
  final double left;
  final double top;
  final Size tamanio;
  final String textBotton;
  final String text;
  final bool isCode;
  final VoidCallback onPressed;

  PolygomForm({
    Key key,
    @required this.left,
    @required this.top,
    @required this.tamanio,
    @required this.textBotton,
    @required this.isCode,
    @required this.text,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Positioned(
      left: this.left,
      top: this.top,
      child: ClipPath(
        clipper: PolygonRegisterClipper(),
        child: Container(
          width: this.tamanio.width,
          height: this.tamanio.height,
          color: Theme.of(context).primaryColor,
          padding: EdgeInsets.only(
            left: this.left,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment:
                (isCode) ? CrossAxisAlignment.start : CrossAxisAlignment.center,
            children: <Widget>[
              TextWidget(
                width: 168,
                letterSpacing: .7,
                textAlign: TextAlign.center,
                alignment: Alignment.center,
                text: this.text,
                fontSize: (isCode) ? _responsive.ip(1.67) : _responsive.ip(1.4),
                fontWeight: FontWeight.w800,
                maxLines: 2,
              ),
              SizedBox(
                  height: (isCode) ? _responsive.hp(2.2) : _responsive.hp(6)),
              Container(
                width: 200,
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: (isCode)
                      ? MainAxisAlignment.start
                      : MainAxisAlignment.center,
                  children: (isCode)
                      ? List.generate(
                          codigo.length, (i) => InputCodigo(number: codigo[i]))
                      : List.generate(
                          numbers.length,
                          (i) => ItemPhone(
                            maxLength: (i == 0) ? 3 : 12,
                            ancho: (i == 0) ? 43 : 128,
                            number: numbers[i],
                            padding: (i == 0) ? 20 : 0,
                          ),
                        ),
                ),
              ),
              SizedBox(
                  height: (isCode) ? _responsive.hp(2.2) : _responsive.hp(6)),
              RoundedButton(
                padding: EdgeInsets.only(
                  left: (isCode) ? _responsive.wp(3) : 0,
                ),
                text: this.textBotton,
                onPressed: this.onPressed,
                width: _responsive.wp(35),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
