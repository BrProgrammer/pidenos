import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/register/widgets/item_register.dart';
import 'package:pidenos/src/utils/data.dart';
import 'package:pidenos/src/widgets/slideshow.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print(this);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Slideshow(
            slides: List.generate(
              registers.length,
              (i) => Consumer<SlideshowModel>(
                builder: (context, slide, _) {
                  return ItemRegister(
                    title: registers[i].title,
                    description: registers[i].description,
                    textBotton: registers[i].textBotton,
                    text: registers[i].text,
                    isCode: registers[i].isCode,
                    size: Size(
                      ((i == 0 || i == 2) ? size.width : size.height) *
                          registers[i].width,
                      ((i == 0 || i == 2) ? size.width : size.height) *
                          registers[i].height,
                    ),
                    top: size.height * registers[i].top,
                    left: size.width * registers[i].left,
                    onPressed: () => (slide.paginaActual < 1)
                        ? slide.paginaActual = slide.paginaActual + 1
                        : Navigator.pushReplacementNamed(context, 'home'),
                  );
                },
              ),
            ).toList(),
            isRegister: true,
            colorPrimario: Theme.of(context).accentColor,
            colorSecundario: Theme.of(context).primaryColor,
            bulletPrimario: 10,
            bulletSecundario: 10,
          ),
        ),
      ),
    );
  }
}
