import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/data.dart';
import 'package:pidenos/src/widgets/slideshow.dart';

class MySlishow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Slideshow(
        slides: items.map((e) => Image.asset(e)).toList(),
        colorPrimario: Theme.of(context).accentColor,
        colorSecundario: Theme.of(context).primaryColor,
      ),
    );
  }
}
