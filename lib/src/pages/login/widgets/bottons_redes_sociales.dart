import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/rounded_botton.dart';

class BottonsRedesSociales extends StatelessWidget {
  void push(BuildContext context) =>
      Navigator.pushReplacementNamed(context, 'register');

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return Container(
      padding: EdgeInsets.only(bottom: _responsive.hp(4.5)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RoundedButton(
            color: Color(0xff2851C6),
            width: _responsive.hp(17),
            onPressed: () => push(context),
            text: 'Facebook',
            path: 'assets/icons/facebook.png',
          ),
          SizedBox(width: _responsive.hp(2)),
          RoundedButton(
            color: Color(0xffD33B3B),
            width: _responsive.hp(17),
            onPressed: () => push(context),
            text: 'Google',
            path: 'assets/icons/google.png',
          ),
        ],
      ),
    );
  }
}
