import 'package:flutter/material.dart';
import 'package:pidenos/src/clippers/form_clippers.dart';
import 'package:pidenos/src/pages/login/widgets/bottons_redes_sociales.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/input_text_login.dart';
import 'package:pidenos/src/widgets/rounded_botton.dart';
import 'package:pidenos/src/widgets/text_widget.dart';

class FormLogin extends StatelessWidget {
  void push(BuildContext context) =>
      Navigator.pushReplacementNamed(context, 'register');

  @override
  Widget build(BuildContext context) {
    final _responsive = Responsive.of(context);
    return ClipPath(
      clipper: FormClipper(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Theme.of(context).primaryColor,
        child: Container(
          margin: EdgeInsets.only(top: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: TextWidget(
                  text: 'Iniciar sesión',
                  fontSize: _responsive.ip(2.5),
                ),
              ),
              InputTextLogin(
                width: 220,
                iconPath: 'assets/icons/user.png',
                placeHolder: 'Correo electrónico',
                textInputType: TextInputType.emailAddress,
              ),
              SizedBox(height: _responsive.hp(2.5)),
              InputTextLogin(
                width: 220,
                iconPath: 'assets/icons/candado.png',
                placeHolder: 'Contraseña',
                isOscure: true,
              ),
              TextWidget(
                text: '¿Olvidaste tu contraseña?',
                fontSize: _responsive.ip(1.3),
                padding: EdgeInsets.symmetric(vertical: _responsive.hp(2)),
                fontWeight: FontWeight.w500,
              ),
              RoundedButton(
                onPressed: () => push(context),
                text: 'Iniciar Sesión',
                padding: EdgeInsets.symmetric(vertical: _responsive.hp(1)),
                width: _responsive.hp(26),
              ),
              TextWidget(
                text: 'Inicia sesión con tu cuenta',
                fontSize: _responsive.ip(1.4),
                padding: EdgeInsets.symmetric(vertical: _responsive.hp(2)),
                fontWeight: FontWeight.w500,
              ),
              BottonsRedesSociales(),
            ],
          ),
        ),
      ),
    );
  }
}
