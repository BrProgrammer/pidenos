import 'package:flutter/material.dart';
import 'package:pidenos/src/utils/responsive.dart';
import 'package:pidenos/src/widgets/input_text_login.dart';

class ItemPhone extends StatelessWidget {
  final double ancho;
  final String number;
  final double padding;
  final int maxLength;

  ItemPhone({
    Key key,
    @required this.ancho,
    @required this.number,
    @required this.padding,
    @required this.maxLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        InputTextLogin(
          width: this.ancho,
          initValue: this.number,
          maxLength: this.maxLength,
          textInputType: TextInputType.phone,
        ),
        if (this.maxLength < 4) SizedBox(width: Responsive.of(context).wp(5)),
      ],
    );
  }
}
