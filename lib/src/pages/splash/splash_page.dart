import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/splash/splash_bloc.dart';
import 'package:pidenos/src/blocs/splash/splash_state.dart';
import 'package:pidenos/src/pages/splash/widgets/body_splash.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  final SplashBloc _splashBloc = SplashBloc();

  @override
  void dispose() {
    _splashBloc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      body: BlocProvider(
        create: (_) => _splashBloc,
        child: BlocListener<SplashBloc, SplashState>(
          listener: (context, state) {
            if (state.status == SplashStatus.loaded) {
              Navigator.pushReplacementNamed(context, 'login');
            }
          },
          child: BodySplash(),
        ),
      ),
    );
  }
}
