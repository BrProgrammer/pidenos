import 'package:flutter/material.dart';
import 'package:pidenos/src/pages/splash/widgets/loading.dart';

class BodySplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Center(
          child: Image.asset(
            'assets/icons/logo.png',
            width: MediaQuery.of(context).size.width * 0.4,
          ),
        ),
        Positioned(
          bottom: 20,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            child: Loading(),
          ),
        ),
      ],
    );
  }
}
