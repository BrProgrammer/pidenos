import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/home/home_event.dart';
import 'package:pidenos/src/blocs/home/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() {
    add(Selecting());
  }

  @override
  HomeState get initialState => HomeState.initialState;

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is Selecting) {
      yield this.state.copywith(status: HomeStatus.selecting);
    } else if (event is Recomendations) {
      yield this.state.copywith(
            status: HomeStatus.recomendations,
            cercas: event.cercas,
            resenias: event.resenias,
            tittleSelection: event.tittleSelection,
          );
    }
  }

  static HomeBloc of(BuildContext context) =>
      BlocProvider.of<HomeBloc>(context);
}
