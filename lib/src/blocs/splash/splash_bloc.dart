import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pidenos/src/blocs/splash/splash_event.dart';
import 'package:pidenos/src/blocs/splash/splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() {
    add(LoadingEvent());
  }

  @override
  SplashState get initialState => SplashState.initialState;

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is LoadingEvent) {
      await Future.delayed(Duration(seconds: 5));
      yield this.state.copywith(status: SplashStatus.loaded);
    }
  }
}
