import 'package:flutter/material.dart';

class CardsClipper extends CustomClipper<Path> {
  final double border;

  CardsClipper({@required this.border});

  @override
  Path getClip(Size size) => Path()
    ..lineTo(0, size.height * .33)
    ..lineTo(0, size.height - this.border)
    ..quadraticBezierTo(0, size.height, this.border, size.height)
    ..lineTo(size.width - this.border, size.height)
    ..quadraticBezierTo(
        size.width, size.height, size.width, size.height - this.border)
    ..lineTo(size.width, this.border)
    ..quadraticBezierTo(size.width, 0, size.width - this.border, 0)
    ..lineTo(this.border, 0)
    ..quadraticBezierTo(0, 0, 0, this.border)
    ..lineTo(0, size.height - this.border)
    ..close();

  @override
  bool shouldReclip(CardsClipper oldClipper) =>
      this.border != oldClipper.border;
}
