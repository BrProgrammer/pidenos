import 'package:flutter/material.dart';
import 'package:pidenos/src/clippers/polygon_panel/polygon_path_drawer.dart';

class ClipPolygon extends StatelessWidget {
  final Widget child;
  final double borderRadius;

  ClipPolygon({
    @required this.child,
    this.borderRadius: 0.0,
  });

  @override
  Widget build(BuildContext context) => ClipPath(
        clipper: Polygon(
          PolygonPathSpecs(
            sides: 6,
            borderRadiusAngle: borderRadius,
          ),
        ),
        child: this.child,
      );
}

class Polygon extends CustomClipper<Path> {
  final PolygonPathSpecs specs;

  Polygon(this.specs);

  @override
  Path getClip(Size size) {
    return PolygonPathDrawer(size: size, specs: specs).draw();
  }

  @override
  bool shouldReclip(Polygon oldClipper) => oldClipper.specs != specs;
}
