import 'package:flutter/material.dart';

class FormClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => Path()
    ..lineTo(0, size.height * 0.28)
    ..lineTo(size.width * 0.25, size.height * 0.01)
    ..arcToPoint(
      Offset(size.width * 0.3, 0),
      radius: Radius.circular(40),
    )
    ..lineTo(size.width * 0.7, 0)
    ..arcToPoint(
      Offset(size.width * 0.75, size.height * 0.01),
      radius: Radius.circular(40),
    )
    ..lineTo(size.width, size.height * 0.28)
    ..lineTo(size.width, size.height)
    ..lineTo(0, size.height)
    ..close();

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
