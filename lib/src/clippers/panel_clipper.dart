import 'package:flutter/material.dart';

class PanelClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => Path()
    ..lineTo(size.width * .72, 0)
    ..arcToPoint(Offset(size.width * .75, size.height * .009),
        radius: Radius.circular(15))
    ..lineTo(size.width * .991, size.height * .47)
    ..arcToPoint(Offset(size.width, size.height * .5),
        radius: Radius.circular(15))
    ..lineTo(size.width * .75, size.height * .991)
    ..arcToPoint(Offset(size.width * .72, size.height),
        radius: Radius.circular(15))
    ..lineTo(size.width * .25, size.height)
    ..arcToPoint(Offset(size.width * .22, size.height * .995),
        radius: Radius.circular(15))
    ..lineTo(0, size.height * .525)
    ..lineTo(0, size.height * .5)
    ..arcToPoint(Offset(0, size.height * .5), radius: Radius.circular(15))
    ..lineTo(size.width * .25, 0)
    ..close();
  // ..close();
  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
