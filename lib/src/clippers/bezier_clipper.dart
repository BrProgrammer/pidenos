import 'package:flutter/material.dart';

class BezierClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Offset p0 = Offset(0, size.height * 0.7);
    final Offset p1 = Offset(size.width * 0.5, size.height * 1.1);
    final Offset p2 = Offset(size.width, size.height * 0.7);
    final path = Path();

    path.lineTo(p0.dx, p0.dy);
    path.quadraticBezierTo(p1.dx, p1.dy, p2.dx, p2.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
