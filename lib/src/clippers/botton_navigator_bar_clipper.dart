import 'package:flutter/material.dart';

class BottonNavigatorClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => Path()
    ..lineTo(size.width * 0.42, 0)
    ..arcToPoint(
      Offset(size.width * 0.58, 0),
      radius: Radius.circular(33),
      clockwise: false,
    )
    ..lineTo(size.width, 0)
    ..lineTo(size.width, size.height)
    ..lineTo(0, size.height)
    ..close();

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
